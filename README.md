# Skippy-XD switcher

## Purpose

This BASH script can be assigned to a keyboard shortcut in order to launch the
Skippy-XD window tiling program.  It looks for other instances of Skippy, and
closes them if they exist.  If no other sessions of Skippy exist, it will launch
one for desktop access.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_skippy_xd.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_skippy_xd/src/7634069c4b5eeda3fa4965cba6cb34fa8a75c2d5/LICENSE.txt?at=master) file for
details.

